/**
 * 
 */
package Scenario;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.xml.internal.ws.policy.spi.AssertionCreationException;

/**
 * @author Елена
 *
 */
public class ScenarioTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Тест метода для {@link Scenario.Scenario#Scenario(Scenario.BDInterface)}.
	 */
	@Test
	public void testScenario() {
	}

	/**
	 * Тест метода для {@link Scenario.Scenario#frameOpenOrClose(int, boolean)}.
	 */
	@Test
	public void testFrameOpenOrClose1() {
		System.out.println("FrameOpenOrClose1");
		Scenario scenario;
		String expResult = "";
		try {
			scenario = new Scenario(new BDRealisation());
			scenario.frameOpenOrClose(7, true);
			fail();
		}
		catch (Exception e) {
			expResult = e.getMessage();
		}
        assertEquals(expResult, "The frame 7 not exists");
	}

	@Test
	public void testFrameOpenOrClose2() {
		System.out.println("FrameOpenOrClose2");
		Scenario scenario;
		String expResult = "";
		try {
			scenario = new Scenario(new BDRealisation());
			scenario.frameOpenOrClose(1, true);
			fail();
		}
		catch (Exception e) {
			expResult = e.getMessage();
		}
        assertEquals(expResult, "The frame 1 is off");
	}	
	
	@Test
	public void testFrameOpenOrClose3() {
		System.out.println("FrameOpenOrClose3");
		Scenario scenario;
		String expResult = "";
		try {
			scenario = new Scenario(new BDRealisation());
			scenario.frameOpenOrClose(2, false);
			fail();
		}
		catch (Exception e) {
			expResult = e.getMessage();
		}
        assertEquals(expResult, "The frame 2 is already has this state");
	}

	@Test
	public void testFrameOpenOrClose4() {
		System.out.println("FrameOpenOrClose4");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.frameOpenOrClose(2, true);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}

	@Test
	public void testFrameOpenOrClose5() {
		System.out.println("FrameOpenOrClose5");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.frameOpenOrClose(4, false);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}
	
	/**
	 * Тест метода для {@link Scenario.Scenario#frameOnOrOff(int, boolean)}.
	 */
	@Test
	public void testFrameOnOrOff1() {
		System.out.println("FrameOnOrOff1");
		Scenario scenario;
		String expResult = "";
		try {
			scenario = new Scenario(new BDRealisation());
			scenario.frameOnOrOff(7, true);
			fail();
		}
		catch (Exception e) {
			expResult = e.getMessage();
		}
        assertEquals(expResult, "The frame 7 not exists");
	}

	@Test
	public void testFrameOnOrOff2() {
		System.out.println("FrameOnOrOff2");
		Scenario scenario;
		String expResult = "";
		try {
			scenario = new Scenario(new BDRealisation());
			scenario.frameOnOrOff(2, true);
			fail();
		}
		catch (Exception e) {
			expResult = e.getMessage();
		}
		assertEquals(expResult, "The frame 2 is already has this state");
	}
	
	@Test
	public void testFrameOnOrOff3() {
		System.out.println("FrameOnOrOff3");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.frameOnOrOff(3, true);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}
	
	@Test
	public void testFrameOnOrOff4() {
		System.out.println("FrameOnOrOff4");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.frameOnOrOff(1, true);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}
	
	@Test
	public void testFrameOnOrOff5() {
		System.out.println("FrameOnOrOff5");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.frameOnOrOff(4, false);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}
 
	/**
	 * Тест метода для {@link Scenario.Scenario#WorkingProcess(int, int)}.
	 */
	@Test
	public void testWorkingProcess1() {
		System.out.println("WorkingProcess1");
		Scenario scenario;
		String expResult = "";
		try {
			scenario = new Scenario(new BDRealisation());
			scenario.WorkingProcess(7, 1);
			fail();
		}
		catch (Exception e) {
			expResult = e.getMessage();
		}
        assertEquals(expResult, "The frame 7 not exists");
	}
	
	@Test
	public void testWorkingProcess2() {
		System.out.println("WorkingProcess2");
		Scenario scenario;
		String expResult = "";
		try {
			scenario = new Scenario(new BDRealisation());
			scenario.WorkingProcess(1, 1);
			fail();
		}
		catch (Exception e) {
			expResult = e.getMessage();
		}
        assertEquals(expResult, "The frame 1 is off");
	}
	
	@Test
	public void testWorkingProcess3() {
		System.out.println("WorkingProcess3");
		Scenario scenario;
		String expResult = "";
		try {
			scenario = new Scenario(new BDRealisation());
			scenario.WorkingProcess(2, 5);
			fail();
		}
		catch (Exception e) {
			expResult = e.getMessage();
		}
        assertEquals(expResult, "The ticket 5 not exists");
	}
	
	@Test
	public void testWorkingProcess4() {
		System.out.println("WorkingProcess4");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.WorkingProcess(2, 1);
		}
		catch (Exception e) {}
		assertEquals(expResult, result);
	}
	
	@Test
	public void testWorkingProcess5() {
		System.out.println("WorkingProcess5");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.WorkingProcess(4, 2);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}
	
	@Test
	public void testWorkingProcess6() {
		System.out.println("WorkingProcess6");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.WorkingProcess(5, 3);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}
	
	@Test
	public void testWorkingProcess7() {
		System.out.println("WorkingProcess7");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.WorkingProcess(6, 4);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}

	/**
	 * Тест метода для {@link Scenario.Scenario#StationOnOrOff(int, boolean)}.
	 */
	@Test
	public void testStationOnOrOff1() {
		System.out.println("StationOnOrOff1");
		Scenario scenario;
		String expResult = "";
		try {
			scenario = new Scenario(new BDRealisation());
			scenario.StationOnOrOff(5, true);
			fail();
		}
		catch (Exception e) {
			expResult = e.getMessage();
		}
        assertEquals(expResult, "The station 5 not exists");
	}
	
	@Test
	public void testStationOnOrOff2() {
		System.out.println("StationOnOrOff2");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.StationOnOrOff(1, true);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}
	
	@Test
	public void testStationOnOrOff3() {
		System.out.println("StationOnOrOff3");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.StationOnOrOff(2, true);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}
	
	@Test
	public void testStationOnOrOff4() {
		System.out.println("StationOnOrOff4");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.StationOnOrOff(3, false);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}

	@Test
	public void testStationOnOrOff5() {
		System.out.println("StationOnOrOff5");
		Scenario scenario;
		boolean expResult = true;
		boolean result = false;
		try {
			scenario = new Scenario(new BDRealisation());
			result = scenario.StationOnOrOff(4, true);
		}
		catch (Exception e) {}
        assertEquals(expResult, result);
	}
}
