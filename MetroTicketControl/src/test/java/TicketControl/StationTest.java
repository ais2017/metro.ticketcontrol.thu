package TicketControl;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Елена
 */
public class StationTest {
    public Station st1, st2, st3, st4;
    ArrayList<Frame> fr_list1, fr_list2, fr_list3, fr_list4;
    public Frame frame1, frame2, frame3, frame4, frame5, frame6, frame7, frame8;
    public BillingTicket billticket1, billticket2, billticket3, billticket4;
    ArrayList<BillingTicket> bill_list1, bill_list2, bill_list3, bill_list4;
    public Ticket ticket1, ticket2;
    
    
    public StationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ticket1 = new Ticket(120021, 0);//tariff-money
        ticket2 = new Ticket(230032, 1);//tariff-time
        billticket1 = new BillingTicket(ticket1, true);
        billticket2 = new BillingTicket(ticket2, true);
        billticket3 = new BillingTicket(ticket1, false);
        billticket4 = new BillingTicket(ticket2, false);
        
        bill_list1 = new ArrayList<BillingTicket>();
        bill_list2 = new ArrayList<BillingTicket>();
        bill_list3 = new ArrayList<BillingTicket>();
        bill_list4 = new ArrayList<BillingTicket>();
        
        boolean b = bill_list2.add(billticket1);
        b = bill_list4.add(billticket3);
        
        frame1 = new Frame(1, 0, 1, true, false, bill_list1);
        frame2 = new Frame(2, 1, 0, false, true, bill_list1);
        frame3 = new Frame(3, 0, 1, true, false, bill_list2);
        frame4 = new Frame(4, 0, 0, false, true, bill_list2);
        frame5 = new Frame(5, 2, 2, true, true, bill_list2);
        frame6 = new Frame(6, 0, 0, true, false, bill_list4);
        frame7 = new Frame(7, 1, 1, false, true, bill_list4);
        frame8 = new Frame(8, 2, 0, true, true, bill_list4);

        fr_list1 = new ArrayList<Frame>();
        fr_list2 = new ArrayList<Frame>();
        fr_list3 = new ArrayList<Frame>();
        fr_list4 = new ArrayList<Frame>();
        
        b = fr_list2.add(frame1);
        b = fr_list3.add(frame2);
        b = fr_list3.add(frame3);
        b = fr_list3.add(frame6);
        b = fr_list4.add(frame2);
        b = fr_list4.add(frame4);
        b = fr_list4.add(frame7);
        
        st1 = new Station(1, fr_list1);
        st2 = new Station(2, fr_list2);
        st3 = new Station(3, fr_list3);
        st4 = new Station(4, fr_list4);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of openAllFrames method, of class Station.
     */
    @Test
    public void testOpenAllFrames() {
        System.out.println("openAllFrames");
        st1.openAllFrames();
        st2.openAllFrames();
        st3.openAllFrames();
        st4.openAllFrames();
    }

    /**
     * Test of closeAllFrames method, of class Station.
     */
    @Test
    public void testCloseAllFrames() {
        System.out.println("closeAllFrames");
        st1.closeAllFrames();
        st2.closeAllFrames();
        st3.closeAllFrames();
        st4.closeAllFrames();
    }

    /**
     * Test of addFrame method, of class Station.
     */
    @Test
    public void testAddFrame() {
        System.out.println("addFrame");
        boolean expResult = true;
        boolean result = st1.addFrame(frame1);
        assertEquals(expResult, result);
        
        boolean expResult2 = true;
        boolean result2 = st2.addFrame(frame1);
        assertEquals(expResult2, result2);
        
        boolean expResult3 = true;
        boolean result3 = st3.addFrame(frame1);
        assertEquals(expResult3, result3);
        
        boolean expResult4 = true;
        boolean result4 = st4.addFrame(frame1);
        assertEquals(expResult4, result4);
    }

    /**
     * Test of deleteFrame method, of class Station.
     */
    @Test
    public void testDeleteFrame() {
        System.out.println("deleteFrame");
        boolean expResult = false;
        boolean result = st1.deleteFrame(frame1);
        assertEquals(expResult, result);

        boolean expResult2 = true;
        boolean result2 = st2.deleteFrame(frame1);
        assertEquals(expResult2, result2);
        
        boolean expResult3 = false;
        boolean result3 = st3.deleteFrame(frame1);
        assertEquals(expResult3, result3);
        
        boolean expResult4 = true;
        boolean result4 = st3.deleteFrame(frame3);
        assertEquals(expResult4, result4);
    }
    
    /**
     * Test of getFrames method, of class Station.
     */
    @Test
    public void testGetFrames() {
        System.out.println("getFrames");
        ArrayList<Frame> expResult = fr_list1;
        ArrayList<Frame> result = st1.getFrames();
        assertEquals(expResult, result);
        
        ArrayList<Frame> expResult2 = fr_list2;
        ArrayList<Frame> result2 = st2.getFrames();
        assertEquals(expResult2, result2);
        
        ArrayList<Frame> expResult3 = fr_list3;
        ArrayList<Frame> result3 = st3.getFrames();
        assertEquals(expResult3, result3);
        
        ArrayList<Frame> expResult4 = fr_list4;
        ArrayList<Frame> result4 = st4.getFrames();
        assertEquals(expResult4, result4);
    } 
    
    /**
     * Test of getID method, of class Station.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        int expResult = 1;
        int result = st1.getID();
        assertEquals(expResult, result);
        
        int expResult2 = 2;
        int result2 = st2.getID();
        assertEquals(expResult2, result2);
        
        int expResult3 = 3;
        int result3 = st3.getID();
        assertEquals(expResult3, result3);
        
        int expResult4 = 4;
        int result4 = st4.getID();
        assertEquals(expResult4, result4);
    } 
}
