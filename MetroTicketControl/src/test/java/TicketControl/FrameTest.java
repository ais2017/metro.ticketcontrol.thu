package TicketControl;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Елена
 */
public class FrameTest {
    public Frame frame1, frame2, frame3, frame4, frame5, frame6, frame7, frame8;
    public BillingTicket billticket1, billticket2, billticket3, billticket4;
    ArrayList<BillingTicket> bill_list1, bill_list2, bill_list3, bill_list4;
    public Ticket ticket1, ticket2;
    
    public FrameTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ticket1 = new Ticket(120021, 0);//tariff-money
        ticket2 = new Ticket(230032, 1);//tariff-time
        billticket1 = new BillingTicket(ticket1, true);
        billticket2 = new BillingTicket(ticket2, true);
        billticket3 = new BillingTicket(ticket1, false);
        billticket4 = new BillingTicket(ticket2, false);
        
        bill_list1 = new ArrayList<BillingTicket>();
        bill_list2 = new ArrayList<BillingTicket>();
        bill_list3 = new ArrayList<BillingTicket>();
        bill_list4 = new ArrayList<BillingTicket>();
        
        boolean b2 = bill_list2.add(billticket1);
        boolean b4 = bill_list4.add(billticket3);
        
        frame1 = new Frame(1, 0, 1, true, false, bill_list1);
        frame2 = new Frame(2, 1, 0, false, true, null);
        frame3 = new Frame(3, 0, 1, true, false, bill_list2);
        frame4 = new Frame(4, 0, 0, false, true, bill_list2);
        frame5 = new Frame(5, 2, 2, true, true, bill_list2);
        frame6 = new Frame(6, 0, 0, true, false, bill_list4);
        frame7 = new Frame(7, 1, 1, false, true, bill_list4);
        frame8 = new Frame(8, 2, 0, true, true, bill_list4);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of frameOn method, of class Frame.
     */
    @Test
    public void testFrameOn() {
        System.out.println("frameOn");
        boolean expResult = true;
        boolean result = frame1.frameOn();
        assertEquals(expResult, result);
        
        boolean expResult2 = true;
        boolean result2 = frame2.frameOn();
        assertEquals(expResult2, result2);
    }

    /**
     * Test of frameOff method, of class Frame.
     */
    @Test
    public void testFrameOff() {
        System.out.println("frameOff");
        boolean expResult = false;
        boolean result = frame1.frameOff();
        assertEquals(expResult, result);
        
        boolean expResult2 = false;
        boolean result2 = frame2.frameOff();
        assertEquals(expResult2, result2);
    }

    /**
     * Test of frameOpen method, of class Frame.
     */
    @Test
    public void testFrameOpen() {
        System.out.println("frameOpen");
        boolean expResult = true;
        boolean result = frame1.frameOpen();
        assertEquals(expResult, result);
        
        boolean expResult2 = true;
        boolean result2 = frame2.frameOpen();
        assertEquals(expResult2, result2);
    }

    /**
     * Test of frameClose method, of class Frame.
     */
    @Test
    public void testFrameClose() {
        System.out.println("frameClose");
        boolean expResult = false;
        boolean result = frame1.frameClose();
        assertEquals(expResult, result);
        
        boolean expResult2 = false;
        boolean result2 = frame2.frameClose();
        assertEquals(expResult2, result2);
    }

    /**
     * Test of changeLightSignal method, of class Frame.
     */
    @Test
    public void testChangeLightSignal() {
        System.out.println("changeLightSignal");
        int light = 1;
        int expResult = 1;
        int result = frame1.changeLightSignal(light);
        assertEquals(expResult, result);
        
        int light2 = 2;
        int expResult2 = 2;
        int result2 = frame1.changeLightSignal(light2);
        assertEquals(expResult2, result2);
        
        int light3 = 0;
        int expResult3 = 0;
        int result3 = frame2.changeLightSignal(light3);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of makeLigth_signal method, of class Frame.
     */
    @Test
    public void testMakeLigth_signal() {
        System.out.println("makeLigth_signal");
        int expResult = 0;
        int result = frame1.makeLigth_signal();
        assertEquals(expResult, result);
        
        int expResult2 = 1;
        int result2 = frame3.makeLigth_signal();
        assertEquals(expResult2, result2);
        
        int expResult3 = 2;
        int result3 = frame6.makeLigth_signal();
        assertEquals(expResult3, result3);
    }

    /**
     * Test of makeSound_signal method, of class Frame.
     */
    @Test
    public void testMakeSound_signal() {
        System.out.println("makeSound_signal");
        int expResult = 2;
        int result = frame1.makeSound_signal();
        assertEquals(expResult, result);
        
        int expResult2 = 0;
        int result2 = frame3.makeSound_signal();
        assertEquals(expResult2, result2);
        
        int expResult3 = 1;
        int result3 = frame6.makeSound_signal();
        assertEquals(expResult3, result3);
    }

    /**
     * Test of addBillingTicket method, of class Frame.
     */
    @Test
    public void testAddBillingTicket() {
        System.out.println("addBillingTicket");
        boolean expResult = false;
        boolean result = frame3.addBillingTicket(billticket1);
        assertEquals(expResult, result);

        boolean expResult2 = true;
        boolean result2 = frame2.addBillingTicket(billticket1);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of deleteBillingTicket method, of class Frame.
     */
    @Test
    public void testDeleteBillingTicket() {
        System.out.println("deleteBillingTicket");
        boolean expResult = false;
        boolean result = frame1.deleteBillingTicket(billticket1);
        assertEquals(expResult, result);
        
        boolean expResult2 = true;
        boolean result2 = frame3.deleteBillingTicket(billticket1);
        assertEquals(expResult2, result2);
        
        boolean expResult3 = false;
        boolean result3 = frame2.deleteBillingTicket(billticket1);
        assertEquals(expResult3, result3);
    }
    
    /**
     * Test of getOn_off method, of class Frame.
     */
    @Test
    public void testGetOn_off() {
        System.out.println("getOn_off");
        boolean expResult = true;
        boolean result = frame1.getOn_off();
        assertEquals(expResult, result);
        
        boolean expResult2 = false;
        boolean result2 = frame2.getOn_off();
        assertEquals(expResult2, result2);
    } 
    
    /**
     * Test of getID method, of class Frame.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        int expResult = 1;
        int result = frame1.getID();
        assertEquals(expResult, result);
        
        int expResult2 = 2;
        int result2 = frame2.getID();
        assertEquals(expResult2, result2);
    } 
    
    /**
     * Test of getOpen_close method, of class Frame.
     */
    @Test
    public void testGetOpen_close() {
        System.out.println("getOpen_close");
        boolean expResult = false;
        boolean result = frame1.getOpen_close();
        assertEquals(expResult, result);
        
        boolean expResult2 = true;
        boolean result2 = frame2.getOpen_close();
        assertEquals(expResult2, result2);
    } 
    
    /**
     * Test of getBilling_ticket method, of class Frame.
     */
    @Test
    public void TestGetBilling_ticket() {
        System.out.println("getBilling_ticket");
        ArrayList<BillingTicket> expResult = bill_list1;
        ArrayList<BillingTicket> result = frame1.getBilling_ticket();
        assertEquals(expResult, result);
        
        ArrayList<BillingTicket> expResult2 = bill_list2;
        ArrayList<BillingTicket> result2 = frame3.getBilling_ticket();
        assertEquals(expResult2, result2);
    } 
    
    /**
     * Test of setSound_signal method, of class Frame.
     */
    @Test
    public void testSetSound_signal() {
        System.out.println("setSound_signal");
        frame1.setSound_signal(0);
        frame2.setSound_signal(1);
        frame3.setSound_signal(2);
    }
}
