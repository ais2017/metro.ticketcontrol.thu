package TicketControl;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.*;

import java.util.Calendar;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Елена
 */
public class BillingTicketTest {
    public BillingTicket billticket1, billticket2, billticket3, billticket4;
    public Ticket ticket1, ticket2;
    int money1 = 70;
    int money2 = 20;
    int cost = 50;
    
    public BillingTicketTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ticket1 = new Ticket(120021, 0);//tariff-money
        ticket2 = new Ticket(230032, 1);//tariff-time
        billticket1 = new BillingTicket(ticket1, true);
        billticket2 = new BillingTicket(ticket2, true);
        billticket3 = new BillingTicket(ticket1, false);
        billticket4 = new BillingTicket(ticket2, false);
        
        Calendar date1 = Calendar.getInstance();
        date1.setTimeZone(TimeZone.getTimeZone("UTC"));
        date1.set(2017, Calendar.DECEMBER, 30, 0, 0, 0);
        Calendar date2 = Calendar.getInstance();
        date2.setTimeZone(TimeZone.getTimeZone("UTC"));
        date2.set(2017, Calendar.OCTOBER, 30, 0, 0, 0);

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of changeValidity method, of class BillingTicket.
     */
    @Test
    public void testDetermineValidity() {
        System.out.println("determineValidity");
        boolean expResult = true;
        Calendar date1 = Calendar.getInstance();
        date1.setTimeZone(TimeZone.getTimeZone("UTC"));
        date1.set(2017, Calendar.DECEMBER, 30, 0, 0, 0);
        boolean result = billticket1.determineValidity(date1, money1, cost);
        assertEquals(expResult, result);
    }
        
    /**
     * Test of changeValidity method, of class BillingTicket.

     */
    @Test
    public void testDetermineValidity1() {
        System.out.println("determineValidity1");
        boolean expResult2 = false;
        Calendar date1 = Calendar.getInstance();
        date1.setTimeZone(TimeZone.getTimeZone("UTC"));
        date1.set(2017, Calendar.DECEMBER, 30, 0, 0, 0);
        boolean result2 = billticket1.determineValidity(date1, money2, cost);
        assertEquals(expResult2, result2);
    }
    
     /**
     * Test of changeValidity method, of class BillingTicket.
     */
    @Test
    public void testDetermineValidity2() {
        System.out.println("determineValidity2");
        boolean expResult3 = false;
        Calendar date2 = Calendar.getInstance();
        date2.setTimeZone(TimeZone.getTimeZone("UTC"));
        date2.set(2017, Calendar.OCTOBER, 30, 0, 0, 0);
        boolean result3 = billticket2.determineValidity(date2, money1, cost);
        assertEquals(expResult3, result3);
    }
    /**
     * Test of changeValidity method, of class BillingTicket.
     */
    @Test
    public void testDetermineValidity3() {
        System.out.println("determineValidity3");
        boolean expResult4 = true;
        Calendar date1 = Calendar.getInstance();
        date1.setTimeZone(TimeZone.getTimeZone("UTC"));
        date1.set(2017, Calendar.DECEMBER, 30, 0, 0, 0);
        boolean result4 = billticket2.determineValidity(date1, money2, cost);
        assertEquals(expResult4, result4);
    }

    /**
     * Test of updateTicketBill method, of class BillingTicket.
     */
    @Test
    public void testUpdateTicketBill() {
        System.out.println("updateTicketBill");
        int expResult = 20;
        int result = billticket1.updateTicketBill(money1, cost);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of updateTicketBill method, of class BillingTicket.
     */
    @Test
    public void testUpdateTicketBill1() {
        System.out.println("updateTicketBill1");
        int expResult = -30;
        int result = billticket1.updateTicketBill(money2, cost);
        assertEquals(expResult, result);
    }

    /**
     * Test of getValidity method, of class BillingTicket.
     */
    @Test
    public void testGetValidity() {
        System.out.println("getValidity");
        boolean expResult = true;
        boolean result = billticket1.getValidity();
        assertEquals(expResult, result);
        
        boolean expResult2 = true;
        boolean result2 = billticket2.getValidity();
        assertEquals(expResult2, result2);
        
        boolean expResult3 = false;
        boolean result3 = billticket3.getValidity();
        assertEquals(expResult3, result3);
        
        boolean expResult4 = false;
        boolean result4 = billticket4.getValidity();
        assertEquals(expResult4, result4);
    }
    
}
