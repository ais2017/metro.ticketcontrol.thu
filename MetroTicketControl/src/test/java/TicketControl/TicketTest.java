package TicketControl;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Елена
 */
public class TicketTest {
    public Ticket ticket1, ticket2;
    public TicketTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ticket1 = new Ticket(120021, 0);//tariff-money
        ticket2 = new Ticket(230032, 1);//tariff-time
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRfid method, of class Ticket.
     */
    @Test
    public void testGetRfid() {
        System.out.println("getRfid");
        int expResult = 120021;
        int result = ticket1.getRfid();
        assertEquals(expResult, result);
        
        int expResult2 = 230032;
        int result2 = ticket2.getRfid();
        assertEquals(expResult2, result2);
    }
    
     /**
     * Test of getTariff method, of class Ticket.
     */
    @Test
    public void testGetTafiff() {
        System.out.println("getTafiff");
        int expResult = 0;
        int result = ticket1.getTariff();
        assertEquals(expResult, result);
        
        int expResult2 = 1;
        int result2 = ticket2.getTariff();
        assertEquals(expResult2, result2);
    } 
}
