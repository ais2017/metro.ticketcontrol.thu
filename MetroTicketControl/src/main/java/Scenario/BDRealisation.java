/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenario;

import TicketControl.Frame;
import TicketControl.Station;
import TicketControl.Ticket;

import java.util.*;

/**
 *
 * @author Елена
 */
 
public class BDRealisation implements BDInterface {
    private Vector<Frame> vFrame = new Vector<Frame>();
    private Vector<Ticket> vTicket = new Vector<Ticket>();
    private Map<Ticket, TicketInfo> mTicketInfo = new HashMap<Ticket, TicketInfo>();
    private Vector<Station> vStation = new Vector<Station>();
//    private Vector<BillingTicket> vBillingTicket = new Vector<BillingTicket>();

    public BDRealisation() throws Exception {
        //билеты
        vTicket.addElement(new Ticket(1,0));   //денежный тариф
        vTicket.addElement(new Ticket(2,0));   //денежный тариф
        vTicket.addElement(new Ticket(3,1));   //временной тариф
        vTicket.addElement(new Ticket(4,1));   //временной тариф

        //информация о билетах
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

        calendar1.set(2017, Calendar.DECEMBER, 31, 0, 0, 0);
        calendar2.set(2017, Calendar.OCTOBER, 1, 0, 0, 0);

        mTicketInfo.put(vTicket.get(0), new TicketInfo(calendar1, 70, 50));  //денежный тариф, валиден
        mTicketInfo.put(vTicket.get(1), new TicketInfo(calendar1, 30, 50));  //денежный тариф, не валиден
        mTicketInfo.put(vTicket.get(2), new TicketInfo(calendar1, 70, 50));  //временной тариф, валиден
        mTicketInfo.put(vTicket.get(3), new TicketInfo(calendar2, 70, 50));  //временной тариф, не валиден

        //биллинговые системы
//        vBillingTicket.addElement(new BillingTicket(null, false));    //без билета
//        vBillingTicket.addElement(new BillingTicket(vTicket.get(0), false));    //денежный билет, валиден
//        vBillingTicket.addElement(new BillingTicket(vTicket.get(1), false));    //денежный билет, не валиден
//        vBillingTicket.addElement(new BillingTicket(vTicket.get(2), false));    //временной тариф, валиден
//        vBillingTicket.addElement(new BillingTicket(vTicket.get(3), false));    //временной тариф, не валиден
        //рамки
        //ArrayList<BillingTicket> abilling = new ArrayList<BillingTicket>();

        vFrame.addElement(new Frame(1, 3, 0, false, false, null));  //синий, выключена, закрыта
        vFrame.addElement(new Frame(2, 3, 0, true, false, null));  //синий, включена, закрыта
        vFrame.addElement(new Frame(3, 0, 2, false, true, null));  //красный, звук2, выключена, открыта
        vFrame.addElement(new Frame(4, 0, 2, true, true, null));  //красный, звук2, включена, открыта
        vFrame.addElement(new Frame(5, 1, 0, true, true, null));  //зеленый, включена, открыта
        vFrame.addElement(new Frame(6, 2, 1, true, true, null));  //желтый, звук1, включена, открыта

        //станции
        ArrayList<Frame> frameList1 = new ArrayList<Frame>();
        ArrayList<Frame> frameList2 = new ArrayList<Frame>();
        frameList2.add(vFrame.get(0));

        ArrayList<Frame> frameList3 = new ArrayList<Frame>();
        frameList3.add(vFrame.get(1));
        frameList3.add(vFrame.get(2));

        ArrayList<Frame> frameList4 = new ArrayList<Frame>();
        frameList4.add(vFrame.get(3));
        frameList4.add(vFrame.get(4));
        frameList4.add(vFrame.get(5));

        vStation.addElement(new Station(1, frameList1));    //станция без рамок
        vStation.addElement(new Station(2, frameList2));    //станция с рамкой 1(синий, выключена, закрыта)
        vStation.addElement(new Station(3, frameList3));    //станция с рамками 2(синий, включена, закрыта), 3(красный, звук2, выключена, открыта)
        vStation.addElement(new Station(4, frameList4));    //станция с рамками 4(красный, звук2, включена, открыта), 
                                                            //5(красный, звук2, выключена, открыта), 6(желтый, звук1, включена, открыта)
    }
    
   // @Override
    public Calendar getTicketDate(Ticket ticket) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i) == ticket)
                return mTicketInfo.get(vTicket.get(i)).ticketDate;
        return null;
    }

  //  @Override
    public int getBill(Ticket ticket) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i) == ticket)
                return mTicketInfo.get(vTicket.get(i)).bill;
        return 0;
    }

   // @Override
    public int getTravelCost(Ticket ticket) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i) == ticket)
                return mTicketInfo.get(vTicket.get(i)).travelCost;
        return 0;
    }

  //  @Override
    public void setBill(Ticket ticket, int bill) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i) == ticket){
                TicketInfo tf = mTicketInfo.get(vTicket.get(i));
                tf.bill = bill;
                mTicketInfo.put(ticket, tf);
            }
    }

   // @Override
    public Frame getFrameByID(int frameID) {
        for (int i=0; i<vFrame.size(); i++)
            if (vFrame.get(i).getID() == frameID)
                return vFrame.get(i);
        return null;
    }

  //  @Override
    public Ticket getTicketByID(int ticketID) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i).getRfid() == ticketID)
                return vTicket.get(i);
        return null;
    }

  //  @Override
    public Station getStationByID(int stationID) {
        for (int i=0; i<vStation.size(); i++)
            if (vStation.get(i).getID() == stationID)
                return vStation.get(i);
        return null;
    }
}
