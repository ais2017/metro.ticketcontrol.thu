/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenario;

import java.util.Calendar;

/**
 *
 * @author Елена
 */
public class TicketInfo {
    public Calendar ticketDate;
    public int bill;
    public int travelCost; 
    
    public TicketInfo(Calendar ticketDate, int bill, int travelCost){
        this.ticketDate = ticketDate;
        this.bill = bill;
        this.travelCost = travelCost;
    }
}
