/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenario;

import TicketControl.BillingTicket;
import TicketControl.Frame;
import TicketControl.Station;
import TicketControl.Ticket;
import static java.lang.Thread.sleep;
import java.util.Calendar;

/**
 *
 * @author Елена
 */
public class Scenario {
    private BDInterface myInterface;

    public Scenario(BDInterface myInterface) {
        this.myInterface = myInterface;
    }
    
    public boolean frameOpenOrClose(int frameID, boolean open_close) throws Exception {
        Frame frame = myInterface.getFrameByID(frameID);
        if (frame == null)
            throw new Exception("The frame " + frameID + " not exists");
        
        //проверка способности выполнения
        if (!frame.getOn_off()) //рамка выключена
            throw new Exception("The frame " + frameID + " is off");

        if (frame.getOpen_close() == open_close)    //статус нельзя изменить
            throw new Exception("The frame " + frameID + " is already has this state");

        //статус можно изменить
        if (open_close) {   //сигнал открытия
            frame.changeLightSignal(1); //зеленый
            frame.frameOpen();
            System.out.println("The frame " + frameID + " has been opened with green ligth signal\n");
        }
        else {  //сигнал закрытия
            frame.changeLightSignal(3); //синий
            frame.frameClose();
            System.out.println("The frame " + frameID + " has been closed with blue ligth signal\n"); 
        }
        return true;
    }
    
    public boolean frameOnOrOff(int frameID, boolean on_off) throws Exception {
        Frame frame = myInterface.getFrameByID(frameID);
        if (frame == null)
            throw new Exception("The frame " + frameID + " not exists");
        
        //проверка способности выполнения
        if (frame.getOn_off() == on_off) //статус нельзя изменить
            throw new Exception("The frame " + frameID + " is already has this state");

        if (on_off) {   //сигнал включения
            frame.frameOn();
            if (frame.getOpen_close()) {  //рамка открыта
                frame.changeLightSignal(1); //зеленый
                System.out.println("The frame " + frameID + " has been turned on and opened with green ligth signal\n");
            }
            else {   //рамка закрыта
                frame.changeLightSignal(3); //синий
                System.out.println("The frame " + frameID + " has been turned on and opened with blue ligth signal\n");
            }
        }
        else {  //сигнал выключения
            frame.frameOff();
            System.out.println("The frame " + frameID + " has been turned off\n");

        }
        return true;
    }
    
    public boolean WorkingProcess(int frameID, int ticketID) throws Exception {
        Frame frame = myInterface.getFrameByID(frameID);

        if (frame == null) {
        	System.out.println("The frame " + frameID + " not exists");
            throw new Exception("The frame " + frameID + " not exists");
        }
        
        //рамка может не работать, надо проверить
        if (!frame.getOn_off()) {
        	System.out.println("The frame " + frame.getID() + " is off");
            throw new Exception("The frame " + frame.getID() + " is off");
        }
        
        Ticket ticket = myInterface.getTicketByID(ticketID);

        if (ticket != null) {   
            //фиксация билета в рамке
            BillingTicket billingTicket;
            billingTicket = new BillingTicket(ticket, false);
            frame.addBillingTicket(billingTicket);
                    
            //получаем информацию о билете из бд
            Calendar ticketDate = myInterface.getTicketDate(ticket);
            int money = myInterface.getBill(ticket);
            int cost = myInterface.getTravelCost(ticket);
 
            //определяем валидность билета
            if (frame.getBilling_ticket().get(0).determineValidity(ticketDate, money, cost)) {
                //билет валиден, фиксация прохода:
                //списывание денег со счета
                if (ticket.getTariff() == 0) {
                    money = frame.getBilling_ticket().get(0).updateTicketBill(money, cost);
                    myInterface.setBill(ticket, money);
                }
                frame.frameOpen();  //открыть рамку
            }
            else {
                //билет не валиден
                System.out.println("The ticket " + ticket.getRfid() + " is not valid!\n");
                frame.frameClose(); //закрыть рамку
            }

            frame.makeLigth_signal();   //устанавливаем световой сигнал
            frame.makeSound_signal();   //устанавливаем звуковой сигнал
            sleep(3);   //время на проход/посмотреть на световой сигнал/послушать звук
            
            //закрыть рамку после прохода
            if (frame.getOpen_close())
                frame.frameClose(); 
                
            frame.changeLightSignal(3); //синий цвет в состояние ожидания
            frame.setSound_signal(0);   //убираем звуковой сигнал
            
            //фиксация окончания прохода
            frame.deleteBillingTicket(frame.getBilling_ticket().get(0));    
            
        }
        else { //безбилетник
            System.out.println("The ticket " + ticketID + " is not exist");
            frame.changeLightSignal(0); //красный цвет 
            frame.setSound_signal(2);   //звуковой сигнал
            sleep(3);	//время на посмотреть на световой сигнал/послушать звук
            frame.changeLightSignal(3); //синий цвет в состояние ожидания
            frame.setSound_signal(0);   //убираем звуковой сигнал
                throw new Exception("The ticket " + ticketID + " not exists");
        }
        return true;
    }
    
    public boolean StationOnOrOff(int stationID, boolean on_off) throws Exception {
    
        Station station = myInterface.getStationByID(stationID);
        if (station == null)
            throw new Exception("The station " + stationID + " not exists");
        
        if (on_off) {   //сигнал включения
            station.openAllFrames();
            System.out.println("All frames in station " + station.getID() + " has been turned on\n");
            for (int i=0; i<station.getFrames().size(); i++){
                if (station.getFrames().get(i).getOpen_close()) {   //рамка открыта
                    station.getFrames().get(i).changeLightSignal(1);    //зеленый
                    System.out.println("The frame " + station.getFrames().get(i).getID() + " has been turned on and opened with green ligth signal\n");
                }
                else {
                    station.getFrames().get(i).changeLightSignal(3); //синий
                    System.out.println("The frame " + station.getFrames().get(i).getID() + " has been turned on and opened with blue ligth signal\n");
                }
            }
        }
        else {  //сигнал выключения
            station.closeAllFrames();
            System.out.println("All frames in station " + station.getID() + " has been turned off\n");
        }
        return true;
    }
}