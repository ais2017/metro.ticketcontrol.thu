/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenario;

import TicketControl.Frame;
import TicketControl.Station;
import TicketControl.Ticket;

import java.util.Calendar;

/**
 *
 * @author baran
 */
public interface BDInterface {
    
    public Calendar getTicketDate(TicketControl.Ticket ticket);
    public int getBill(TicketControl.Ticket ticket);
    public int getTravelCost(TicketControl.Ticket ticket);
    public void setBill(TicketControl.Ticket ticket, int bill);
    
    public Frame getFrameByID(int frameID);
    public Ticket getTicketByID(int ticketID);
    public Station getStationByID(int stationID);
}
