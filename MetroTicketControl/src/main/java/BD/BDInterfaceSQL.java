/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import TicketControl.Frame;
import TicketControl.Station;
import TicketControl.Ticket;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;

/**
 *
 * @author baran
 */
public interface BDInterfaceSQL {

    public Calendar getTicketDate(Ticket ticket);
    public int getBill(Ticket ticket);
    public int getTravelCost(Ticket ticket);
    public void setBill(Ticket ticket, int bill);
    public boolean updateTickInfo(Integer id) throws SQLException;
    
    public Frame getFrameByID(int frameID);
    public boolean addFrame(int station) throws SQLException;
    public boolean delFrame(int station, int frameID) throws SQLException;
    public boolean updateFrame(Integer id_fr) throws SQLException;
    public boolean updateFrameStation(Integer id_st) throws SQLException;

    public Ticket getTicketByID(int ticketID);
    public Vector<Ticket> getTicketList();

    public Station getStationByID(int stationID);
    public Integer addStation() throws SQLException;
    public boolean delStation(int station) throws SQLException;
    public Vector<Station> getStationlist();

    public  void select(Connection con) throws SQLException;
}
