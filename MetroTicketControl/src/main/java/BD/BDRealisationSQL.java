/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import Scenario.TicketInfo;
import TicketControl.Frame;
import TicketControl.Station;
import TicketControl.Ticket;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Driver;
import com.mysql.jdbc.Statement;

import javax.swing.text.StyledEditorKit;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 *
 * @author Елена
 */
 
public class BDRealisationSQL implements BDInterfaceSQL {
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";
    private static final String URL = "jdbc:mysql://localhost:3306/TicketControl";

    Connection connection = null;
    Driver driver;
    Statement statement;

    private Vector<Frame> vFrame = new Vector<Frame>();
    private Vector<Ticket> vTicket = new Vector<Ticket>();
    private Map<Ticket, TicketInfo> mTicketInfo = new HashMap<Ticket, TicketInfo>();
    private Vector<Station> vStation = new Vector<Station>();

    public BDRealisationSQL() throws Exception {

        try {
            connection = (Connection) DriverManager.getConnection(URL,USERNAME,PASSWORD);
            select(connection);

        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
        connection.close();
    }
    
   // @Override
    public Calendar getTicketDate(Ticket ticket) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i) == ticket)
                return mTicketInfo.get(vTicket.get(i)).ticketDate;
        return null;
    }

  //  @Override
    public int getBill(Ticket ticket) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i) == ticket)
                return mTicketInfo.get(vTicket.get(i)).bill;
        return 0;
    }

   // @Override
    public int getTravelCost(Ticket ticket) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i) == ticket)
                return mTicketInfo.get(vTicket.get(i)).travelCost;
        return 0;
    }

  //  @Override
    public void setBill(Ticket ticket, int bill) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i) == ticket){
                TicketInfo tf = mTicketInfo.get(vTicket.get(i));
                tf.bill = bill;
                mTicketInfo.put(ticket, tf);
            }
    }

   // @Override
    public Frame getFrameByID(int frameID) {
        for (int i=0; i<vFrame.size(); i++)
            if (vFrame.get(i).getID() == frameID)
                return vFrame.get(i);
        return null;
    }

  //  @Override
    public Ticket getTicketByID(int ticketID) {
        for (int i=0; i<vTicket.size(); i++)
            if (vTicket.get(i).getRfid() == ticketID)
                return vTicket.get(i);
        return null;
    }

    //  @Override
    public TicketInfo getTicketInfoByID(int tickInfoID) {
        Object Obj1, Obj2;
        Ticket ticket;
        TicketInfo ticketInfo;

        for (Map.Entry entry : mTicketInfo.entrySet()) {

            Obj1 = entry.getKey();
            Obj2 = entry.getValue();

            ticket = (Ticket) Obj1;
            ticketInfo = (TicketInfo) Obj2;

            if(ticket.getRfid() == tickInfoID)
                return ticketInfo;

            Obj1 = null; Obj2 = null; ticket = null; ticketInfo = null;
        }

        return null;
    }

    //@Override
    public Vector<Ticket> getTicketList() {
        if(!vTicket.isEmpty())
            return vTicket;
        else
            return null;
    }

    //  @Override
    public Station getStationByID(int stationID) {
        for (int i=0; i<vStation.size(); i++)
            if (vStation.get(i).getID() == stationID)
                return vStation.get(i);
        return null;
    }

    public Integer addStation() throws SQLException {
        Connection con = null;
        Integer contactId = -1;
        con = (Connection) DriverManager.getConnection(URL,USERNAME,PASSWORD);

        if(con != null) {
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO stations (title) VALUES (?)", new String[] {"id"});

            stmt.setString(1, "Novaya");
            stmt.executeUpdate();

            ResultSet gk = stmt.getGeneratedKeys();
            if(gk.next()) {
                contactId = gk.getInt(1);
                Station station =  new Station(contactId-1,new ArrayList<Frame>());
                vStation.add(station);
            }
            stmt.close();
            con.close();
        }
        return contactId - 1;
    }

    public boolean delStation(int station) throws SQLException {
        Connection con = null;
        con = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
        if (con != null) {
            PreparedStatement stmt = con.prepareStatement(
                    "DELETE FROM stations WHERE id = ?");

            stmt.setInt(1, station + 1);
            stmt.executeUpdate();

            ArrayList<Frame> fr = getStationByID(station).getFrames();

            for (int i = 0; i < vFrame.size(); i++)
                for (int j = 0; j < fr.size(); j++)
                    if (vFrame.get(i).getID() == fr.get(j).getID()) {
                        vFrame.remove(i);
                        break;
                    }
            vStation.remove(getStationByID(station));
            stmt.close();
            con.close();
            return true;
        }
        return false;
    }

    public Vector<Station> getStationlist() {
        if(!vStation.isEmpty())
           return vStation;
        else
            return null;
    }

    public void select(java.sql.Connection con) throws SQLException {

        //Получили данные из БД по Билетам
        Statement stmt = (Statement) con.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM tickets");

        while (rs.next()) {
            Integer id = rs.getInt("id");
            Integer tarif = rs.getInt("tarif");

            vTicket.addElement(new Ticket(id - 1,tarif));   //денежный тариф
        }
        rs.close();
        stmt.close();

        //Получили данные из БД по информации о Билетах
        stmt = (Statement) con.createStatement();
        rs = stmt.executeQuery("SELECT * FROM ticketinfo");

        while (rs.next()) {

            String [] str = rs.getString("date_tick").split("-");
            Integer money = rs.getInt("money");
            Integer travcost = rs.getInt("travelCost");
            Integer id_ticket = rs.getInt("id_tick");

            Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Integer.valueOf(str[0]), Integer.valueOf(str[1]), Integer.valueOf(str[2]));

            //System.out.print("Calen: " + calendar1.get(Calendar.YEAR) + "_" + calendar1.get(Calendar.MONTH) + "_" + calendar1.get(Calendar.DAY_OF_MONTH) + "_" + str[1] +"\n");

            for(Ticket f : vTicket)
                if(f.getRfid() == (id_ticket - 1))
                    mTicketInfo.put(f, new TicketInfo(calendar1, money, travcost));
        }
        rs.close();
        stmt.close();

        //Получили данные из БД по Рамкам
        stmt = (Statement) con.createStatement();
        rs = stmt.executeQuery("SELECT * FROM frames");
        Map<Frame, Integer> buf = new HashMap<Frame, Integer>();

        while (rs.next()) {
            Integer id = rs.getInt("id");
            Integer id_station = rs.getInt("id_st");
            Integer light = rs.getInt("light");
            Integer sound = rs.getInt("sound");
            Boolean on_off = rs.getBoolean("on_off");
            Boolean open_close = rs.getBoolean("open_close");

            Frame frame =  new Frame(id-1, light, sound, on_off, open_close, null);
            buf.put(frame, id_station-1);
            vFrame.addElement(frame);
        }
        rs.close();
        stmt.close();


        //Получили данные из БД по Станциям
        stmt = (Statement) con.createStatement();
        rs = stmt.executeQuery("SELECT * FROM stations");

        while (rs.next()) {
            ArrayList<Frame> frameList = new ArrayList<Frame>();
            Integer id = rs.getInt("id");
            id --;

            for(Map.Entry<Frame, Integer> entry : buf.entrySet())
                if(id.equals(entry.getValue()))
                    frameList.add(entry.getKey());

            vStation.addElement(new Station(id, frameList));
        }
        rs.close();
        stmt.close();
    }

    public boolean updateFrame(Integer id_fr) throws SQLException {
        Connection con = null;
        con = (Connection) DriverManager.getConnection(URL,USERNAME,PASSWORD);
        Frame frame = getFrameByID(id_fr);

        Integer light = frame.getLight_signal();
        Integer sound = frame.getSound_signal();
        Boolean on_off = frame.getOn_off();
        Boolean op_cl = frame.getOpen_close();

        if(con != null) {
            PreparedStatement stmt = con.prepareStatement(
                    "UPDATE frames SET light = ?, sound = ?, on_off = ?, open_close = ? WHERE id = ?");

            stmt.setInt(1, light);
            stmt.setInt(2, sound);
            stmt.setBoolean(3, on_off);
            stmt.setBoolean(4, op_cl);
            stmt.setInt(5, id_fr + 1);

            stmt.executeUpdate();
            stmt.close();
            con.close();
            return true;
        }

        return false;

      /*  Statement  stmt2 = (Statement) con.createStatement();
        ResultSet rs = stmt2.executeQuery("SELECT * FROM frames");

        while (rs.next()) {
            ArrayList<Frame> frameList = new ArrayList<Frame>();
            Integer id = rs.getInt("id");
            Integer ids = rs.getInt("id_st");
            Boolean fll = rs.getBoolean("on_off");

            System.out.printf("ssssssssss %d %d ",ids, id);
            System.out.print(fll + "\n");
        }
        rs.close();
        stmt2.close();*/
    }

    public boolean updateFrameStation(Integer id_st) throws SQLException {
        Connection con = null;
        con = (Connection) DriverManager.getConnection(URL,USERNAME,PASSWORD);

        Station station = getStationByID(id_st);

        if(con != null) {
            con.setAutoCommit(false);

            for(int i = 0; i < station.getFrames().size(); i++)
            {
                Frame frame = station.getFrames().get(i);

                Integer id_fr = frame.getID();
                Integer light = frame.getLight_signal();
                Integer sound = frame.getSound_signal();
                Boolean on_off = frame.getOn_off();
                Boolean op_cl = frame.getOpen_close();

                PreparedStatement stmt = con.prepareStatement(
                        "UPDATE frames SET light = ?, sound = ?, on_off = ?, open_close = ? WHERE id = ?");

                stmt.setInt(1, light);
                stmt.setInt(2, sound);
                stmt.setBoolean(3, on_off);
                stmt.setBoolean(4, op_cl);
                stmt.setInt(5, id_fr + 1);

                stmt.executeUpdate();
                stmt.close();
            }
            con.commit();
            con.setAutoCommit(true);
            con.close();
            return true;
        }
        return false;
    }

    public boolean delFrame(int station, int frameID) throws SQLException {
        Connection con = null;
        con = (Connection) DriverManager.getConnection(URL,USERNAME,PASSWORD);
        if(con != null) {
            PreparedStatement stmt = con.prepareStatement(
                    "DELETE FROM frames WHERE id = ?");

            stmt.setInt(1, frameID + 1);
            stmt.executeUpdate();

            getStationByID(station).deleteFrame(getFrameByID(frameID));

            for (int i=0; i<vFrame.size(); i++)
                if (vFrame.get(i).getID() == frameID)
                     vFrame.remove(i);

            stmt.close();
            con.close();
            return true;
        }
        return false;
    }

    public boolean addFrame(int station)throws SQLException {
        Connection con = null;
        Integer contactId = 0;
        con = (Connection) DriverManager.getConnection(URL,USERNAME,PASSWORD);

        if(con != null) {
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO frames (id_st, light, sound, on_off, open_close) VALUES (?, ?, ?, ?, ?)", new String[] {"id"});

            stmt.setInt(1, station + 1);
            stmt.setInt(2, 3);
            stmt.setInt(3, 0);
            stmt.setBoolean(4, true);
            stmt.setBoolean(5, false);
            stmt.executeUpdate();

            ResultSet gk = stmt.getGeneratedKeys();
            if(gk.next()) {
                contactId = gk.getInt(1);
                Frame frame =  new Frame(contactId-1, 3, 0, true, false, null);
                vFrame.addElement(frame);
                getStationByID(station).addFrame(frame);
            }
            stmt.close();
            con.close();
            return true;
        }
        return false;
    }

    public boolean updateTickInfo(Integer id) throws SQLException {
        Connection con = null;
        con = (Connection) DriverManager.getConnection(URL,USERNAME,PASSWORD);
        //TicketInfo ticketInfo = getTicketInfoByID(id);
        if(con != null) {
            Integer money = getBill(getTicketByID(id));

            PreparedStatement stmt = con.prepareStatement(
                    "UPDATE ticketinfo SET money = ? WHERE id = ?");

            stmt.setInt(1, money);
            stmt.setInt(2, id + 1);

            stmt.executeUpdate();
            stmt.close();
            con.close();
            return true;
        }
        return false;
    }
}