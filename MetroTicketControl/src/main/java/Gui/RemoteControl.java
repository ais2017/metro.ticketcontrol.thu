package Gui;

import BD.BDRealisationSQL;
import Scenario.ScenarioSQL;
//import TicketControl.Frame;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Vector;

public class RemoteControl {
    private JPanel panel1;
    private JComboBox comboBox1;
    private JTable table1;
    private JButton button1;
    private JButton button2;
    private JButton updateButton;
    private JButton remButton;
    private JButton addButton;
    private JTable table2;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JTable table3;
    private JButton button9;
    private JButton button10;
    private JButton button11;
    private JCheckBox onOffCheckBox;
    private JButton updateButton1;
    private JCheckBox onOffCheckBox1;
    private JCheckBox openCloseCheckBox;
    private JButton checkTicketButton;
    private JTextField selectFrameTextField;
    private JTextField selectTicketTextField;
    private JTabbedPane tabbedPane1;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button12;
    private Integer cur_id = -1;

    private ScenarioSQL scenario;

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }


    public RemoteControl() throws Exception {

        scenario = new ScenarioSQL(new BDRealisationSQL());

        //Заполнили станции в comboBox1
        for(int i = 0; i < scenario.getInterf().getStationlist().size(); i++)
            if(scenario.getInterf().getStationlist().get(i).getID() >= 0)
                this.comboBox1.addItem(scenario.getInterf().getStationlist().get(i).getID());

        this.comboBox1.setEditable(false);

        //Заполнили рамки в table1
        Vector<String> headerVect = new Vector<String>();
        headerVect.add("id");
        headerVect.add("light");
        headerVect.add("sound");
        headerVect.add("on_off");
        headerVect.add("open_cl");

        final DefaultTableModel mod = new DefaultTableModel(headerVect, 0){
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };

        this.table1.setModel(mod);
        mod.addRow(headerVect);

        refreshVal(mod);

        //Заполнили билеты в table3
        Vector<String> headerVect2 = new Vector<String>();
        headerVect2.add("id");
        headerVect2.add("tarif");
        headerVect2.add("Date");
        headerVect2.add("Cost");
        headerVect2.add("Money");

        final DefaultTableModel mod2 = new DefaultTableModel(headerVect2, 0){
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };

        this.table3.setModel(mod2);
        mod2.addRow(headerVect2);
        refreshValTicket(mod2);
        this.table3.getColumnModel().getColumn(2).setMinWidth(80);


        //При смене станции в таблице список рамок менятся
        comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               refreshVal(mod);
            }
        });

        //Обработчик кнопок для Станции
        addButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            Integer num = -1;
            try {
                if((num = scenario.getInterf().addStation()) >= 0){
                    comboBox1.addItem(scenario.getInterf().getStationByID(num).getID());
                    refreshVal(mod);
                    JOptionPane.showMessageDialog(null, "Добавили станцию!!!");
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        });

        remButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    if(((Integer) comboBox1.getSelectedItem()) != null)
                        if(scenario.getInterf().delStation((Integer) comboBox1.getSelectedItem())) {
                            comboBox1.removeItem((Integer) comboBox1.getSelectedItem());
                            refreshVal(mod);
                            JOptionPane.showMessageDialog(null, "Удалили станцию!!!");
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

        updateButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(((Integer) comboBox1.getSelectedItem()) != null)
                    if(onOffCheckBox.isSelected()) {
                        try {
                            if (scenario.StationOnOrOff((Integer) comboBox1.getSelectedItem(), true)) {
                                refreshVal(mod);
                                JOptionPane.showMessageDialog(null, "Станция открыта!!!");
                            }
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                    }
                    else {
                        try {
                            if (scenario.StationOnOrOff((Integer) comboBox1.getSelectedItem(), false)) {
                                refreshVal(mod);
                                JOptionPane.showMessageDialog(null, "Станция закрыта!!!");
                            }
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                    }
            }
        });

        //Обработчик кнопок для Рамки
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if((Integer) comboBox1.getSelectedItem() != null)
                try {
                    if(scenario.getInterf().addFrame((Integer) comboBox1.getSelectedItem())){
                        refreshVal(mod);
                        JOptionPane.showMessageDialog(null, "Добавили рамку!!!");
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                int row = table1.getSelectedRow();
                if (row > 0) {
                    String curval = (String) table1.getValueAt( row, 0);
                    cur_id = Integer.valueOf(curval);
                }

                if (((Integer) comboBox1.getSelectedItem()) != null && cur_id >= 0) {
                    try {
                        if(scenario.getInterf().delFrame((Integer) comboBox1.getSelectedItem(), cur_id)) {
                            refreshVal(mod);
                            JOptionPane.showMessageDialog(null, "Удалили рамку!!!");
                        }
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
                else
                    JOptionPane.showMessageDialog(null, "Выберите рамку!!!");
            }
        });

        updateButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                int row = table1.getSelectedRow();
                Boolean flag = false;
                if (row > 0) {
                    String curval = (String) table1.getValueAt( row, 0);
                    cur_id = Integer.valueOf(curval);
                }

                if(cur_id >= 0) {
                    if (onOffCheckBox1.isSelected()) {
                        try {
                            if (scenario.frameOnOrOff(cur_id, true)) {
                                flag = true;
                                JOptionPane.showMessageDialog(null, "Включили рамку!!!");
                            } else
                                JOptionPane.showMessageDialog(null, "Рамка уже включена!!!");

                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    } else {
                        try {
                            if (scenario.frameOnOrOff(cur_id, false)) {
                                flag = true;
                                JOptionPane.showMessageDialog(null, "Выключили рамку!!!");
                            } else
                                JOptionPane.showMessageDialog(null, "Рамка уже выключена!!!");
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    if(openCloseCheckBox.isSelected()) {
                        try {
                            if (scenario.frameOpenOrClose(cur_id, true)) {
                                flag = true;
                                JOptionPane.showMessageDialog(null, "Открыли рамку!!!");
                            }
                            else {
                                    String curval = (String) table1.getValueAt( row, 1);
                                    Boolean cur = Boolean.valueOf(curval);
                                if(cur)
                                    JOptionPane.showMessageDialog(null, "Рамка уже открыта!!!");
                                else
                                    JOptionPane.showMessageDialog(null, "Рамка выключена!!!");
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    else {
                        try {
                            if (scenario.frameOpenOrClose(cur_id, false)) {
                                flag = true;
                                JOptionPane.showMessageDialog(null, "Закрыли рамку!!!");
                            }
                            else {
                                String curval = (String) table1.getValueAt( row, 1);
                                Boolean cur = Boolean.valueOf(curval);
                                if(cur)
                                    JOptionPane.showMessageDialog(null, "Рамка уже закрыта!!!");
                                else
                                    JOptionPane.showMessageDialog(null, "Рамка выключена!!!");

                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }
                else
                    JOptionPane.showMessageDialog(null, "Выберите рамку!!!");

                table1.getSelectionModel().clearSelection();
                cur_id = -1;
                if(flag)
                    refreshVal(mod);
            }
        });

        //Обработчик кнопок для Билета
        button11.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Добавили билет!!!");
            }
        });

        button10.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Удалили билет!!!");
            }
        });

        button9.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Обновили информацию!!!");

            }
        });

        checkTicketButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int row = table1.getSelectedRow();
                int row3 = table3.getSelectedRow();
                Integer cur_id_tick = -1;
                Integer stat;

                if (row > 0) {
                    //String str = (String) table1.getValueAt(row, 0);
                    cur_id = Integer.valueOf((String) table1.getValueAt(row, 0));
                }


                if (row3 > 0)
                    cur_id_tick = Integer.valueOf((String) table3.getValueAt( row3, 0));
                   // cur_id_tick = (Integer) table3.getValueAt( row3, 0);


                if(cur_id >= 0) {
                    try {
                        stat = scenario.WorkingProcess(cur_id, cur_id_tick);
                        if(stat == -2)
                            JOptionPane.showMessageDialog(null, "Билет не валиден!!!", "Тревога.", JOptionPane.ERROR_MESSAGE);
                        if(stat == -1)
                            JOptionPane.showMessageDialog(null, "Прошёл безбилетник!!!", "Тревога.", JOptionPane.ERROR_MESSAGE);
                        if(stat == 0)
                            JOptionPane.showMessageDialog(null, "Рамка выключена!!!","Внимание.",JOptionPane.INFORMATION_MESSAGE);
                        if(stat == 1) {
                            refreshValTicket(mod2);
                            JOptionPane.showMessageDialog(null, "Всё в порядке!!!");
                        }
                        if(stat == -3)
                            JOptionPane.showMessageDialog(null, "В БД данные не записались!!!", "Ошибка.", JOptionPane.ERROR_MESSAGE);

                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                else
                    JOptionPane.showMessageDialog(null, "Выберите рамку!!!", "Внимание.", JOptionPane.INFORMATION_MESSAGE);

                table1.getSelectionModel().clearSelection();
                table3.getSelectionModel().clearSelection();
                cur_id = -1;
            }
        });
    }

    public void refreshVal(DefaultTableModel mod) {

        while(mod.getRowCount() > 1 )
            mod.removeRow(1);
        if((Integer) comboBox1.getSelectedItem() != null)
        for(int i = 0; i < scenario.getInterf().getStationByID((Integer) comboBox1.getSelectedItem()).getFrames().size(); i++){
            Vector<String> newRow = new Vector<String>();
            newRow.add(String.valueOf(scenario.getInterf().getStationByID((Integer) comboBox1.getSelectedItem()).getFrames().get(i).getID()));
            newRow.add(String.valueOf(scenario.getInterf().getStationByID((Integer) comboBox1.getSelectedItem()).getFrames().get(i).getLight_signal()));
            newRow.add(String.valueOf(scenario.getInterf().getStationByID((Integer) comboBox1.getSelectedItem()).getFrames().get(i).getSound_signal()));
            newRow.add(String.valueOf(scenario.getInterf().getStationByID((Integer) comboBox1.getSelectedItem()).getFrames().get(i).getOn_off()));
            newRow.add(String.valueOf(scenario.getInterf().getStationByID((Integer) comboBox1.getSelectedItem()).getFrames().get(i).getOpen_close()));
            mod.addRow(newRow);
        }
    }

    public void refreshValTicket(DefaultTableModel mod) {

        while(mod.getRowCount() > 1 )
            mod.removeRow(1);

        for(int i = 0; i < scenario.getInterf().getTicketList().size(); i++)
        {
            Vector<String> newRow = new Vector<String>();
            newRow.add(String.valueOf(scenario.getInterf().getTicketList().get(i).getRfid()));
            newRow.add(String.valueOf(scenario.getInterf().getTicketList().get(i).getTariff()));
               String date = String.valueOf(scenario.getInterf().getTicketDate(scenario.getInterf().getTicketList().get(i)).get(Calendar.YEAR));
               date = date.concat("-").concat(String.valueOf(scenario.getInterf().getTicketDate(scenario.getInterf().getTicketList().get(i)).get(Calendar.MONTH)));
               date = date.concat("-").concat(String.valueOf(scenario.getInterf().getTicketDate(scenario.getInterf().getTicketList().get(i)).get(Calendar.DAY_OF_MONTH)));
            newRow.add(date);
            newRow.add(String.valueOf(scenario.getInterf().getTravelCost(scenario.getInterf().getTicketList().get(i))));
            newRow.add(String.valueOf(scenario.getInterf().getBill(scenario.getInterf().getTicketList().get(i))));
            mod.addRow(newRow);
        }
    }

    public static void main(String [] argc) throws Exception {
        JFrame frame = new JFrame("Remote Control ©Rodionov");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(new RemoteControl().panel1);
        frame.pack();
        frame.setVisible(true);
        frame.setLocation(400,200);
        frame.setSize(900,600);
    }
}
