/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TicketControl;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author Елена
 */
public class BillingTicket {
    private Ticket ticket;
    private boolean validity;
    
    public BillingTicket (Ticket ticket, boolean validity)
    {
        this.ticket = ticket;
        this.validity = validity;        
    }
    
    public boolean determineValidity (Calendar ticketDate, int money, int cost) 
    {
        Date d = new Date();
        Calendar date = Calendar.getInstance();
        date.setTimeZone(TimeZone.getTimeZone("UTC"));
        date.setTime(d);
        
        if (this.ticket.getTariff() == 0)//money
        {
            validity = money >= cost;
        }
        if (this.ticket.getTariff() == 1)//time
        {
            validity = date.before(ticketDate);
        }
        return validity;
    }
    
    public int updateTicketBill (int money, int cost) 
    {        
        return money-cost;
    }
    
    public boolean getValidity ()
    {
        return validity;
    }
    
}
