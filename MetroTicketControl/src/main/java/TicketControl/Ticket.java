/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TicketControl;

/**
 *
 * @author Елена
 */
public class Ticket {
    private final int rfid;
    private final int tariff; //0-money, 1-time

    public Ticket(int rfid, int tariff) {
        this.rfid = rfid;
        this.tariff = tariff;
    }

    public int getRfid() {
        return rfid;
    }

    public int getTariff() {
        return tariff;
    }
}
