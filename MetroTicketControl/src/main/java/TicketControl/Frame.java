/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TicketControl;

import java.util.ArrayList;

/**
 *
 * @author Елена
 */
public class Frame {

    private final int ID;
    private int light_signal;//0-red; 1-green; 2-yellow; 3-blue
    private int sound_signal;//0-nosound; 1-soundNotValid; 2-soundNotExist
    private boolean on_off;//on-true
    private boolean open_close;//open-true
    private ArrayList<BillingTicket> billing_ticket;

    public Frame(int ID, int light_signal, int sound_signal, boolean on_off, boolean open_close, ArrayList<BillingTicket> billing_ticket) {
        this.ID = ID;
        this.light_signal = light_signal;
        this.sound_signal = sound_signal;
        this.on_off = on_off;
        this.open_close = open_close;
        this.billing_ticket = billing_ticket;
    }

    public int getLight_signal()
    {
        return light_signal;
    }

    public int getSound_signal()
    {
        return sound_signal;
    }

    public boolean frameOn ()
    {
        on_off = true;
        return on_off;
    }
    
    public boolean frameOff ()
    {
        on_off = false;
        return on_off;
    }
    
    public boolean frameOpen ()
    {
        open_close = true;
        return open_close;
    }
    
    public boolean frameClose ()
    {
        open_close = false;
        return open_close;
    }
    
    public int changeLightSignal (int light)
    {
        light_signal = light;
        return light_signal;
    }

    public int makeLigth_signal() 
    {
        if (billing_ticket.isEmpty())
            light_signal = 0;
        else
            if (billing_ticket.get(0).getValidity())
                light_signal = 1;
            else
                light_signal = 2;
        return light_signal;
    }

    public int makeSound_signal() {
        if (billing_ticket.isEmpty())
            sound_signal = 2;
        else
            if (billing_ticket.get(0).getValidity())
                sound_signal = 0;
            else
                sound_signal = 1;
        return sound_signal;
    }
    
    public boolean addBillingTicket(BillingTicket bill_ticket)
    {
        if (billing_ticket == null)
            billing_ticket = new ArrayList<BillingTicket>();

        return billing_ticket.add(bill_ticket);
    }
    
    public boolean deleteBillingTicket (BillingTicket bill_ticket)
    {
        if (billing_ticket == null)
            return false;
        return billing_ticket.remove(bill_ticket);
    }

    public boolean getOn_off() {
        return on_off;
    }

    public int getID() {
        return ID;
    }
    
    public boolean getOpen_close() {
        return open_close;
    }

    public ArrayList<BillingTicket> getBilling_ticket() {
        return billing_ticket;
    }

    public void setSound_signal(int sound_signal) {
        this.sound_signal = sound_signal;
    }
}
