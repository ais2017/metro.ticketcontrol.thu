/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TicketControl;

import java.util.ArrayList;

/**
 *
 * @author Елена
 */
public class Station {
    private final int ID;
    private ArrayList<Frame> frames;

    public Station(int ID, ArrayList<Frame> frames) {
        this.ID = ID;
        this.frames = frames;
    }
    
    public void openAllFrames ()
    {
        for(Frame f : frames)
            f.frameOn();
    }
    
    public void closeAllFrames ()
    {
        for(Frame f : frames)
            f.frameOff();
    }
    
    public boolean addFrame (Frame frame)
    {
        return frames.add(frame);
    }
    
    public boolean deleteFrame (Frame frame)
    {
        if (frames.isEmpty())
            return false;
        return frames.remove(frame);
    }

    public ArrayList<Frame> getFrames() {
        return frames;
    }

    public int getID() {
        return ID;
    }
    
}
